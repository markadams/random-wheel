#!/usr/bin/env python3

import argparse
from time import sleep
from random import random, randrange, shuffle


def get_args():
    parser = argparse.ArgumentParser(description='Spins the wheel and determines a winner')
    parser.add_argument('--players', type=str, nargs='+', help='The list of possible winners',
                        default=['John', 'Sam', 'Bill', 'Sally', 'Zhao'])

    return parser.parse_args()


forward, backward = lambda n: n + 1, lambda n: n - 1


def wheel(iterable, state=0, direction=forward):
    values = list(iterable)
    while True:
        direction = (yield values[state]) or direction
        state = direction(state) % len(values)


friction = lambda v, t: v - v / abs(v) * 5 * random() - abs(v) / (2 + random()) * t


def velocity(start=500, stop=0.25, friction=friction):
    state = start
    while abs(state) > stop:
        yield state
        state = friction(state, 1 / state)


def spin(wheel, velocity):
    next(wheel)  # ugly
    for vel in velocity:
        yield wheel.send(lambda n: n + int(vel))
        sleep(1 / abs(vel))


if __name__ == '__main__':
    args = get_args()
    players = args.players
    max_len = max({len(player) for player in players}) + 2

    shuffle(players)

    state, velocity = randrange(0, len(players)), velocity(randrange(500, 750))

    for player in spin(wheel(players, state=state), velocity=velocity):
        print(('| {:>' + str(max_len) + '}  |').format(player))

    print('The winner is... {}'.format(player))
