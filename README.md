Random Wheel for Random Drawings
=============================================
Based on James Powell's excellent presentation from PyTexas 2013:
https://github.com/dutc/generators

Modified slightly by myself to sort through a randomly provided list of names

## Usage
python3 wheel.py --players Mark John Sam Bill Sally
